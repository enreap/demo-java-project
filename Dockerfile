FROM openjdk
WORKDIR /
COPY target/*.jar / 
EXPOSE 8080
CMD java - jar my-app-1.0-SNAPSHOT.jar
ENTRYPOINT ["tail", "-f", "/dev/null"]